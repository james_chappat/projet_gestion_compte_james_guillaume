<?php

function prog_comptes ($A, $B, $C, $Q) {
    return "$A\n$B\n$C\n$Q\n";
}

$valeur = prog_comptes(
    "A-	Création de Compte Courant",
    "B-	Création de Livret A",
    "C-	Création de PEL",
    "Q- Quitter"
);

$liste_de_comptes = [];

$solde_compte_courant = 200 ;
$solde_livret_A = 1000 ;
$solde_PEL = 5000 ;



while (true) {
    echo ($valeur . "\n");
    $choix = readline("Choisissez le type de compte à créer : ");
    $choix = strtoupper($choix);

    if ($choix === "A") {
        if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
            echo ("Le nombre de comptes maximum est déjà atteint. \n");
        break; 
        }
        if ($liste_de_comptes["code compte courant"] != NULL) {
            echo ("Un compte courant est déjà ouvert pour ce client. \n");
            break;
        }
        else {
            $chiffres = "01234567890123456789";
            $compte_courant = str_shuffle($chiffres) ;
            $compte_courant = substr($compte_courant, 1, 11) ;
            $code_compte_courant = $compte_courant ;

            $liste_de_comptes["code compte courant"] = $code_compte_courant;

            echo ("$code_compte_courant \n");
        }

        $decouvert_compte_courant = (bool)readline("Le découvert est-il autorisé ? ");
        if ($decouvert_PEL === "o") {
            $decouvert_PEL = true;
        }
        else {
            $decouvert_PEL = false;
        }

        $liste_de_comptes["Découvert compte courant"] = $decouvert_compte_courant;
        
        /*
        $frais_compte_courant = 25 ;
        $solde_compte_courant == $solde_compte_courant - $frais_compte_courant ;
        $liste_de_comptes["solde compte courant"] = $solde_compte_courant;

        echo ("$solde_compte_courant \n");
        */

    }

    elseif ($choix === "B") {
        if ($liste_de_comptes["code livret A"] != NULL) {
            echo ("Un livret A est déjà ouvert pour ce client. \n");
            break;
        }
        if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
            echo ("Le nombre de comptes maximum est déjà atteint. \n");
        break; 
        }
        else {
            $chiffres = "01234567890123456789";
            $livret_A = str_shuffle($chiffres);
            $livret_A = substr($livret_A, 1, 11);
            $code_livret_A = $livret_A ;

            $liste_de_comptes ["code livret A"] = $code_livret_A ;

            echo ("$code_livret_A \n");
        }

        $decouvert_livret_A = (bool)readline("Le découvert est-il autorisé ? ");
        if ($decouvert_livret_A === "o") {
            $decouvert_livret_A = true;
        }
        else {
            $decouvert_livret_A = false;
        }

        
        $liste_de_comptes["Découvert livret A"] = $decouvert_livret_A ;


        /*

        $frais_livret_A = 25 + ($solde_livret_A * (1 + 10/100)) ;
        $solde_livret_A == $solde_livret_A - $frais_livret_A ;
        $liste_de_comptes["solde livret A"] = $solde_livret_A;

        echo ("$solde_livret_A \n");
        */


    }

    elseif ($choix === "C") {
        if ($liste_de_comptes["code PEL"] != NULL) {
            echo ("Un PEL est déjà ouvert pour ce client. \n");
            break;
        }
        if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
            echo ("Le nombre de comptes maximum est déjà atteint. \n");
        break; 
        }
        else {
            $chiffres = "01234567890123456789";
            $PEL = str_shuffle($chiffres);
            $PEL = substr($PEL, 1, 11);
            $code_PEL = $PEL ;

            $liste_de_comptes ["code PEL"] = $code_PEL ;

            echo ("$code_PEL \n");
        }

        $decouvert_PEL = (bool)readline("Le découvert est-il autorisé ? ");
        
        if ($decouvert_PEL === "o") {
            $decouvert_PEL = true;
        }
        else {
            $decouvert_PEL = false;
        }

        $liste_de_comptes["Découvert PEL"] = $decouvert_PEL;

        /*
        $frais_PEL = 25 + ($solde_PEL * (1 + 2.5/100)) ;
        $solde_PEL == ($solde_PEL - $frais_PEL) ;
        $liste_de_comptes["solde PEL"] = $solde_PEL;
        echo ("$solde_PEL \n");

        */

    }

    elseif ($choix === "Q") {
        var_dump($liste_de_comptes);
        break ;
    }
}


?>