<?php

/*
PAS DE DOUBLON (client, agence, compte)

COMPTE = - Numéro de compte (11 chiffres)
        - Code agence (3 chiffres)
        - Client 
                ~identifiant client (2 caractères majuscules + 6 chiffres)
                ~nom
                ~prénom
                ~date de naissance
                ~email
        - Solde
        - Découvert autorisé

3 types de compte : - compte courant (frais = 25€/an) 
                    - livret A (frais = frais + 10% de l'épargne par an)
                    - PEL (frais = frais + 2.5% de l'épargne par an)

3 comptes maxi 

AGENCE = - code agence 
        - nom de l'agence
        - adresse

CHOIX 7 = imprime dans un .txt (n° client, nom, prénom, date de naissance ; liste de comptes (avec numéro de compte et solde)

*/
/*
$valeur = prog_banque(
    "1 - Créer une agence",
    "2 - Créer un client",
    "3 - Créer un compte bancaire",
    "4 - Recherche de compte", //numero de compte
    "5 - Recherche de client", //nom, numéro de compte, identifiant client
    "6 - Afficher la liste des comptes d'un client ", //identifiant client
    "7 - Imprimer les infos client", //identifiant client
    "8 - Quitter le programme"
);
*/

include "./ID.php";


function prog_comptes ($A, $B, $C, $Q) {
    return "$A\n$B\n$C\n$Q\n";
}

$agences=[];
$agence=[];
$clients=[];
$client=[];
$liste_de_comptes=[];


/*
$valeur = prog_comptes(
    "A-	Création de Compte Courant",
    "B-	Création de Livret A",
    "C-	Création de PEL",
    "Q- Quitter"
);

echo ($valeur . "\n");
*/


while (true) {
    echo "-------------------------------    MENU BANQUE DWWM_20044   ------------------------------------------------\n";
    

    $ch = readline("Faites votre choix: ");

    if ($ch === "1") {
        while (true) {
            
            echo PHP_EOL;
            $code_agence=readline("Veuillez saisir le code de l'agence (code numérique de 3 chiffres) : ");
            if (strlen($code_agence) == 3 AND is_numeric($code_agence)) {
                $agence["code_agence"] =  (int) $code_agence; 
            break;   
            }
            else {
                echo PHP_EOL;
                echo ("Attention !!! Veuillez saisir un code numérique de 3 chiffres".PHP_EOL);
            }
            }
    
            $nom_agence=readline("Veuillez saisir le nom de l'agence : ");
            $agence["nom_agence"]=$nom_agence; 
    
            $cp_agence=readline("Veuillez saisir de code postal de l'agence : ");
            $agence["cp_agence"]=$cp_agence;
    
            $ville_agence=readline("Veuillez saisir la ville de l'agence : ");
            $agence["ville_agence"]=$ville_agence;
    
            $adresse_agence=readline("Veuillez saisir l'adresse de l'agence : ");
            $agence["adresse_agence"]=$adresse_agence;
    
            $agences[]=$agence;

           

    } elseif ($ch === "2") {
        
        while (true) {
            if (readline("Voulez-vous saisir un nouveau client ? (O)ui\(N)on : ") == "n") {
            break;
            }
            
            $code_en_cours = readline("Veuillez saisir le code de l'agence à laquelle vous souhaitez rattacher le client : ");
            if ($code_en_cours != $agences[$i]) {
                echo "Ce code n'est pas attribué. \n";
                break;
            }
            else {

            $client = [];
            $lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            
            $ID_lettres = str_shuffle($lettres);
            $ID_lettres = substr($ID_lettres, 1, 2);
        
            $chiffres = "0123456789";
            $ID_chiffres = str_shuffle($chiffres);
            $ID_chiffres = substr($ID_chiffres, 1, 6);
        
            $code_client = ($ID_lettres.$ID_chiffres);
        
            $client["identifiant"] = $code_client;

            for ($i = 0 ; $i < $codes_clients[$i] ; $i++) {
                if ($code_client === $codes_clients[$i]) {
                echo("Le code client est déjà attribué.");
                continue ;
                }
            }
            
            echo("L'identifiant du client est le  $code_client \n");
            
            $client["nom"] = readline("Veuillez saisir le nom du client : ");
            $client["prenom"] = readline("Veuillez saisir le prénom du client : ");
            $client["naissance"] = readline ("Veuillez saisir la date de naissance du client : (format jjmmaaaa) : ");
            //format ?
            $client["mail"] = readline("Veuillez saisir l'adresse e-mail du client : ");
            //format ?
            $clients[] = $client;
            $agence[] = $clients;

            
            }
        }

    } elseif ($ch === "3") {

/*
$liste_de_comptes = [];
$clients = [];
$client = [];
*/

/*
$solde_compte_courant = 200 ;
$solde_livret_A = 1000 ;
$solde_PEL = 5000 ;
*/

//demande l'identifiant du client à chaque création de compte

        while (true) {
            $id_creation_compte = readline("Veuillez saisir l'identifiant du client : ");

            if ($id_creation_compte != $client["identifiant"]) {
                echo("identifiant client inexistant. \n");
            break;
            }
            else {

            $choix = readline("Choisissez le type de compte à créer (A - Compte courant / B - Livret A / C - P.E.L. / Q - Quitter) : ");
            $choix = strtoupper($choix);

            if ($choix === "A") {
                if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
                    echo ("Le nombre de comptes maximum est déjà atteint. \n");
                break; 
                }
                if ($liste_de_comptes["code compte courant"] != NULL) {
                    echo ("Un compte courant est déjà ouvert pour ce client. \n");
                    break;
                }
                else {
                    $chiffres = "01234567890123456789";
                    $compte_courant = str_shuffle($chiffres) ;
                    $compte_courant = substr($compte_courant, 1, 11) ;
                    $code_compte_courant = $compte_courant ;

                    $liste_de_comptes["code compte courant"] = $code_compte_courant;

                    echo ("Le numéro du compte courant est le : $code_compte_courant .\n");
                

                    $decouvert_compte_courant = (bool)readline("Le découvert est-il autorisé ? ");
                    if ($decouvert_compte_courant === "o") {
                        $decouvert_compte_courant = true;
                    }
                    else {
                        $decouvert_compte_courant = false;
                    }

                    $liste_de_comptes["Découvert compte courant"] = $decouvert_compte_courant;
                    

                    /*
                    $frais_compte_courant = 25 ;
                    $solde_compte_courant == $solde_compte_courant - $frais_compte_courant ;
                    $liste_de_comptes["solde compte courant"] = $solde_compte_courant;

                    echo ("$solde_compte_courant \n");
                    */
                }
            }

            elseif ($choix === "B") {
                if ($liste_de_comptes["code livret A"] != NULL) {
                    echo ("Un livret A est déjà ouvert pour ce client. \n");
                    break;
                }
                if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
                    echo ("Le nombre de comptes maximum est déjà atteint. \n");
                break; 
                }
                else {
                    $chiffres = "01234567890123456789";
                    $livret_A = str_shuffle($chiffres);
                    $livret_A = substr($livret_A, 1, 11);
                    $code_livret_A = $livret_A ;

                    $liste_de_comptes ["code livret A"] = $code_livret_A ;

                    echo ("Le numéro du livret A est le : $code_livret_A .\n");
                

                    $decouvert_livret_A = (bool)readline("Le découvert est-il autorisé ? ");
                    if ($decouvert_livret_A === "o") {
                        $decouvert_livret_A = true;

                    }
                    else {
                        $decouvert_livret_A = false;

                    }

                    
                    $liste_de_comptes["Découvert livret A"] = $decouvert_livret_A ;


                    /*

                    $frais_livret_A = 25 + ($solde_livret_A * (1 + 10/100)) ;
                    $solde_livret_A == $solde_livret_A - $frais_livret_A ;
                    $liste_de_comptes["solde livret A"] = $solde_livret_A;

                    echo ("$solde_livret_A \n");
                    */

                }
            }

            elseif ($choix === "C") {
                if ($liste_de_comptes["code PEL"] != NULL) {
                    echo ("Un PEL est déjà ouvert pour ce client. \n");
                    break;
                }
                if (($liste_de_comptes["code compte courant"] && $liste_de_comptes["code livret A"] && $liste_de_comptes["code PEL"]) != NULL) {
                    echo ("Le nombre de comptes maximum est déjà atteint. \n");
                break; 
                }
                else {
                    $chiffres = "01234567890123456789";
                    $PEL = str_shuffle($chiffres);
                    $PEL = substr($PEL, 1, 11);
                    $code_PEL = $PEL ;

                    $liste_de_comptes ["code PEL"] = $code_PEL ;

                    echo ("Le numéro du PEL est le : $code_PEL .\n");
                

                $decouvert_PEL = (bool)readline("Le découvert est-il autorisé ? ");
                
                if ($decouvert_PEL === "o") {
                    $decouvert_PEL = true;

                }
                else {
                    $decouvert_PEL = false;

                }

                $liste_de_comptes["Découvert PEL"] = $decouvert_PEL;

                /*
                $frais_PEL = 25 + ($solde_PEL * (1 + 2.5/100)) ;
                $solde_PEL == ($solde_PEL - $frais_PEL) ;
                $liste_de_comptes["solde PEL"] = $solde_PEL;
                echo ("$solde_PEL \n");

                */
                }
            
            $client[] = $liste_de_comptes;

            }

            elseif ($choix === "Q") {
             
                break ;
        
            }
           
            }
        }

    } elseif ($ch === "4") {
        
            
            


    } elseif ($ch === "5") {
        


    } elseif ($ch === "6") {
        


    } else if ($ch === "7") {
        


    } elseif ($ch === "8") {
        break;
    }
}
echo ("Au revoir !\n");

echo("-----------------------------------------------------------");
var_dump($client);
echo("-----------------------------------------------------------");
var_dump($clients);
echo("-----------------------------------------------------------");
var_dump($liste_de_comptes);
echo("----------------------------------------------------------");
var_dump($agence);
echo("----------------------------------------------------------");
var_dump($agences);
echo("-----------------------------------------------------------");
  
?>