<?php

include ("fonctions.php");

$clients=[];
$client=[];
$agence=[];
$agences=[];
$comptes=[];

while (true) {
echo "-------------------------------    MENU BANQUE DWWM_20044   ------------------------------------------------\n";

    echo ("1 - Créer une agence".PHP_EOL);
    echo ("2 - Créer un client".PHP_EOL);
    echo ("3 - Créer un compte bancaire".PHP_EOL);
    echo ("4 - Recherche de compte".PHP_EOL);
    echo ("5 - Recherche de client".PHP_EOL);
    echo ("6 - Afficher la liste des comptes d'un client".PHP_EOL);
    echo ("7 - Imprimer les infos client".PHP_EOL);
    echo ("8 - Quitter le programme".PHP_EOL);
    echo ("\n");
   
    $ch = readline("Faites votre choix : ");


    if ($ch === "1") {
     
   
        $agence["code_agence"]=crea_code_agence ();
        
        if ($agence["code_agence"] === $agences["code_agence"]) {
            echo ("Le code agence est déjà attribué. \n");
            continue;
        }
        else {
        $code_agence=$agence["code_agence"];
        echo PHP_EOL;
        $nom_agence=readline("Veuillez saisir le nom de l'agence : ");
        $agence["nom_agence"]=$nom_agence; 
        $adresse_agence=readline("Veuillez saisir l'adresse de l'agence : ");
        $agence["adresse_agence"]=$adresse_agence;
        $agences[]=$agence;
        var_dump ($agences);
        echo ("L'agence $nom_agence a bien été créée");
        echo PHP_EOL;
        echo ("Agence n° $code_agence");
        echo PHP_EOL;
        }
        }


    if ($ch === "2") {

        while (true) {
            $lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            
            $ID_lettres = str_shuffle($lettres);
            $ID_lettres = substr($ID_lettres, 1, 2);
            $chiffres = "0123456789";
            $ID_chiffres = str_shuffle($chiffres);
            $ID_chiffres = substr($ID_chiffres, 1, 6);
            $code_client = ($ID_lettres.$ID_chiffres);

            if ($code_client === $clients["num_client"]) {
          echo ("L'identifiant est déjà attribué. \n");
          continue;
        }
        else {

            $client["num_client"] = $code_client;
        }
            if (readline("Voulez-vous saisir un nouveau client ? (O)ui\(N)on : ") == "n") {
            break;
            }
            $client = [];
            
            $client["num_client"] = $code_client ;
            $nom_client = readline("Veuillez saisir le nom du client : ");
            $client["nom_client"]=$nom_client;

            $client["prenom"] = readline("Veuillez saisir le prénom du client : ");
            $client["naissance"] = readline ("Veuillez saisir la date de naissance du client : ");
            $client["email"] = readline("Veuillez saisir l'adresse e-mail du client : ");
        
        
            $clients[] = $client;

            var_dump ($client);
            echo("\n");
            echo("\n");
            echo("\n");
            var_dump ($clients);
            echo("\n");
            echo("\n");
            echo ("Le client $nom_client a bien été ajouté");
            echo("\n");
            echo ("Numéro client : $code_client");
            echo("\n");
            echo("\n");
            
        }
    }

        if ($ch === "3") {
            $compte=[];
            $compte["num_compte"]=crea_num_compte ();

            if ($compte["num_compte"]===$comptes["num_compte"]) {
            echo ("Le numéro de compte est déjà attribué. \n");
            continue;
            }
            else {


            $num_compte=$compte["num_compte"];
            echo PHP_EOL;
        }
            $id_client_cherche=readline("Veuillez saisir l'identifiant du client : ");

            foreach ($clients as $i => $client) {
                if ($id_client_cherche==$clients[$i]["num_client"]) {
                    $compte["num_client"]=$clients[$i]["num_client"];
                    $compte["nom_client"]=$clients[$i]["nom_client"];
                }
            }

        

            $id_agence_cherche=readline("Veuillez choisir l'identifiant de l'agence : ");

            foreach ($agences as $j => $agence) {
                if ($id_agence_cherche==$agences[$j]["code_agence"]) {
                    $compte["code_agence"]=$agences[$j]["code_agence"];
                    $compte["nom_agence"]=$agences[$j]["nom_agence"];
                }
            }


            while (true) 
            {

            echo ("Veuillez saisir le type de compte : ".PHP_EOL);
            echo ("Choix 1 - Compte courant".PHP_EOL);
            echo ("Choix 2 - Livret A".PHP_EOL);
            echo ("Choix 3 - Plan Epargne Logement".PHP_EOL);
            $choix_compte=readline("Veuillez faire un choix : ");
            echo (PHP_EOL);


            if ($choix_compte==1) {
                if ($compte["type1_compte_client"] != NULL) {
                    echo ("Un compte courant est déjà ouvert pour ce client. \n");
                    continue;
                }
                else {
            $choix_compte="CC";
            $compte["type1_compte_client"] = $choix_compte;

            $solde1=readline("Veuillez saisir le solde du compte courant : ");
            $compte["solde_CC"]=$solde1; 


            $decouvert_compte_courant = (bool)readline("Le découvert est-il autorisé ? ");
                    if ($decouvert_compte_courant === "o") {
                        $decouvert_compte_courant = true;
                    }
                    else {
                        $decouvert_compte_courant = false;
                    }
            $compte["decouvert_CC"]=$decouvert_compte_courant; 
                }
            }

            elseif ($choix_compte==2) {
                if ($compte["type2_compte_client"] != NULL) {
                    echo ("Un Livret A est déjà ouvert pour ce client. \n");
                    continue;
                }
                else {
            $choix_compte="LA";
            $compte["type2_compte_client"] = $choix_compte;

            $solde2=readline("Veuillez saisir le solde du Livret A : ");
            $compte["solde_LA"]=$solde2; 

            
            $decouvert_LA = (bool)readline("Le découvert est-il autorisé ? ");
                    if ($decouvert_LA === "o") {
                        $decouvert_LA = true;
                    }
                    else {
                        $decouvert_LA = false;
                    }

                    $liste_de_comptes["Découvert livret A"] = $decouvert_LA;
                }
            }

            elseif ($choix_compte==3) {
                if ($compte["type3_compte_client"] != NULL) {
                    echo ("Un PEL est déjà ouvert pour ce client. \n");
                    continue;
                }
                else {

            $choix_compte="PEL";
            $compte["type3_compte_client"] = $choix_compte;

            $solde3=readline("Veuillez saisir le solde du Plan Epargne Logement : ");
            $compte["solde_PEL"]=$solde3; 
            

            $decouvert_PEL = (bool)readline("Le découvert est-il autorisé ? ");
                    if ($decouvert_PEL === "o") {
                        $decouvert_PEL = true;
                    }
                    else {
                        $decouvert_PEL = false;
                    }

                    $liste_de_comptes["Découvert PEL"] = $decouvert_PEL ;
        }
            }
            if (readline("Voulez-vous saisir un nouveau type de compte ? (O)ui\(N)on : ") == "n") {
            break;
            }

            }
            
        
            $comptes[]=$compte;
            //var_dump ($compte);
            echo ("\n");
            var_dump ($comptes);
            echo ("\n");
            $nom_client=$compte["nom_client"];
            echo ("Le compte de $nom_client a bien été créé");
            echo ("\n");
            echo ("Compte n° $num_compte");
            echo ("\n");
            
        } 
    




    if ($ch === "4") {
        $cherche_num_compte=readline("Veuillez choisir le numéro de compte à rechercher : ");
        foreach ($comptes as $i => $compte) {
           
            if ($cherche_num_compte==$comptes[$i]["num_compte"]) {
                var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;
                echo ("Numéro client : " .$compte["num_client"]);
                echo PHP_EOL;
                echo ("Numéro de compte : " .$compte["num_compte"]);
                echo PHP_EOL;
                echo ("Code agence : " .$compte["code_agence"]);
                echo PHP_EOL;
            }
        }
        

    }




    if ($ch === "5") {
        echo PHP_EOL;
        echo "-------------------------------    SOUS MENU ONGLET 5   ------------------------------------------------\n";
        echo ("1 - Recherche de client par son nom".PHP_EOL);
        echo ("2 - Recherche de client par son numéro de compte".PHP_EOL);
        echo ("3 - Recherche de client par son identifiant".PHP_EOL);
        echo ("\n");
   
    $ch = readline("Faites votre choix : ");

    if ($ch === "1") {

        $cherche_nom_client=readline("Veuillez choisir le nom du client à rechercher : ");
    
        foreach ($clients as $i => $client) {
           
            if ($cherche_nom_client==$clients[$i]["nom_client"]) {
                var_dump ($client);
                echo PHP_EOL;
                echo ("Numéro Client : " .$client["num_client"]);
                echo PHP_EOL;
                echo ("Nom : " .$client["nom_client"]);
                echo PHP_EOL;
                echo ("Prénom : " .$client["prenom"]);
                echo PHP_EOL;
                echo ("Date de naissance : " .$client["naissance"]);
                echo PHP_EOL;
                echo ("Mail : " .$client["email"]);
                echo PHP_EOL;
            }
        }
    }

    if ($ch === "2") {

        $cherche_num_compte=readline("Veuillez choisir le numéro de compte du client à rechercher : ");
    
        foreach ($comptes as $i => $compte) {
           
            if ($cherche_num_compte==$comptes[$i]["num_compte"]) {
                //var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;
                echo ("Numéro client : " .$compte["num_client"]);
                echo PHP_EOL;
                echo ("Numéro de compte : " .$compte["num_compte"]);
                echo PHP_EOL;
                echo ("Code agence : " .$compte["code_agence"]);
                echo PHP_EOL;
            }
        }

        foreach ($clients as $i => $client) {
           
            if ($cherche_nom_client==$clients[$i]["nom_client"]) {
                //var_dump ($client);
                //echo PHP_EOL;
                //echo ("Numéro Client : " .$client["num_client"]);
                //echo PHP_EOL;
                //echo ("Nom : " .$client["nom_client"]);
                //echo PHP_EOL;
                echo ("Prénom : " .$client["prenom"]);
                echo PHP_EOL;
                echo ("Date de naissance : " .$client["naissance"]);
                echo PHP_EOL;
                echo ("Mail : " .$client["email"]);
                echo PHP_EOL;
            }
        }
    }

    if ($ch === "3") {

        $cherche_num_client=readline("Veuillez choisir le numéro du client à rechercher : ");
    
        foreach ($clients as $i => $client) {
           
            if ($cherche_num_client==$clients[$i]["num_client"]) {
                var_dump ($client);
                echo PHP_EOL;
                echo ("Numéro Client : " .$client["num_client"]);
                echo PHP_EOL;
                echo ("Nom : " .$client["nom_client"]);
                echo PHP_EOL;
                echo ("Prénom : " .$client["prenom"]);
                echo PHP_EOL;
                echo ("Date de naissance : " .$client["naissance"]);
                echo PHP_EOL;
                echo ("Mail : " .$client["email"]);
                echo PHP_EOL;
            }
        }
    }

    }



    if ($ch === "6") {
        $cherche_num_client=readline("Pour afficher la liste des comptes d'un client, veuillez saisir son identifiant : ");
        foreach ($comptes as $i => $compte) {
            if ($cherche_num_client==$comptes[$i]["num_client"]) {
                var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;

                
                echo PHP_EOL;

                if (isset($compte["type1_compte_client"])) {
                    echo ("Compte 1 : ");
                    echo ($compte["type1_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_CC"]);
                    echo (" euros");
                    
                }
                else{
                    
                }

                echo PHP_EOL;

                if (isset($compte["type2_compte_client"])) {
                    echo ("Compte 2 : ");
                    echo ($compte["type2_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_LA"]);
                    echo (" euros");
                   
                }
                else{
                    
                }

                echo PHP_EOL;

                if (isset($compte["type3_compte_client"])) {
                    echo ("Compte 3 : ");
                    echo ($compte["type3_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_PEL"]);
                    echo (" euros");
                    
                }
                else{
                    
                }

                

                echo PHP_EOL;

                
            }
        }
    }




    if ($ch=="8") {
        echo ("Au revoir");
        exit;
    }


}


?>