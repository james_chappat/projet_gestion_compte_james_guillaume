<?php

include ("fonctions.php");

$agences=[];
$comptes=[];
while (true) {
echo "-------------------------------    MENU BANQUE DWWM_20044   ------------------------------------------------\n";

    echo ("1 - Créer une agence".PHP_EOL);
    echo ("2 - Créer un client".PHP_EOL);
    echo ("3 - Créer un compte bancaire".PHP_EOL);
    echo ("4 - Recherche de compte".PHP_EOL);
    echo ("5 - Recherche de client".PHP_EOL);
    echo ("6 - Afficher la liste des comptes d'un client".PHP_EOL);
    echo ("7 - Imprimer les infos client".PHP_EOL);
    echo ("8 - Quitter le programme".PHP_EOL);
    echo ("\n");
   
    $ch = readline("Faites votre choix : ");


    if ($ch === "1") {
     
   
        $agence["code_agence"]=crea_code_agence ();
        $code_agence=$agence["code_agence"];
        echo PHP_EOL;
        $nom_agence=readline("Veuillez saisir le nom de l'agence : ");
        $agence["nom_agence"]=$nom_agence; 
        $adresse_agence=readline("Veuillez saisir l'adresse de l'agence : ");
        $agence["adresse_agence"]=$adresse_agence;
        $agences[]=$agence;
        var_dump ($agences);
        echo ("L'agence $nom_agence a bien été créée");
        echo PHP_EOL;
        echo ("Agence n° $code_agence");
        echo PHP_EOL;
    }  


    if ($ch === "2") {



        while (true) {
            $lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            
            $ID_lettres = str_shuffle($lettres);
            $ID_lettres = substr($ID_lettres, 1, 2);
            $chiffres = "0123456789";
            $ID_chiffres = str_shuffle($chiffres);
            $ID_chiffres = substr($ID_chiffres, 1, 6);
            $code_client = ($ID_lettres.$ID_chiffres);
            $client["num_client"] = $code_client;
            
            //var_dump ($code_client);
            if (readline("Voulez-vous saisir un nouveau client ? (O)ui\(N)on : ") == "n") {
            break;
            }
            $client = [];
            
            $client["num_client"] = $code_client ;
            $nom_client = readline("Veuillez saisir le nom du client : ");
            $client["nom_client"]=$nom_client;

            $client["prenom"] = readline("Veuillez saisir le prénom du client : ");
            $client["naissance"] = readline ("Veuillez saisir la date de naissance du client : ");
            $client["email"] = readline("Veuillez saisir l'adresse e-mail du client : ");
        
        
            $clients[] = $client;

            var_dump ($client);
            echo("\n");
            echo("\n");
            echo("\n");
            var_dump ($clients);
            echo("\n");
            echo("\n");
            echo ("Le client $nom_client a bien été ajouté");
            echo("\n");
            echo ("Numéro client : $code_client");
            echo("\n");
            echo("\n");
        
        }
    }

        if ($ch === "3") {
            $compte=[];
            $compte["num_compte"]=crea_num_compte ();
            $num_compte=$compte["num_compte"];
            echo PHP_EOL;
            $id_client_cherche=readline("Veuillez saisir l'identifiant du client : ");

            foreach ($clients as $i => $client) {
                if ($id_client_cherche==$clients[$i]["num_client"]) {
                    $compte["num_client"]=$clients[$i]["num_client"];
                    $compte["nom_client"]=$clients[$i]["nom_client"];
                }
            }

        

            $id_agence_cherche=readline("Veuillez choisir l'identifiant de l'agence : ");

            foreach ($agences as $j => $agence) {
                if ($id_agence_cherche==$agences[$j]["code_agence"]) {
                    $compte["code_agence"]=$agences[$j]["code_agence"];
                    $compte["nom_agence"]=$agences[$j]["nom_agence"];
                }
            }


            while (true) 
            {

            echo ("Veuillez saisir le type de compte : ".PHP_EOL);
            echo ("Choix 1 - Compte courant".PHP_EOL);
            echo ("Choix 2 - Livret A".PHP_EOL);
            echo ("Choix 3 - Plan Epargne Logement".PHP_EOL);
            $choix_compte=readline("Veuillez faire un choix : ");
            echo (PHP_EOL);


            if ($choix_compte==1) {
            $choix_compte="CC";
            $compte["type1_compte_client"] = $choix_compte;
            $compte["num_compte_CC"]=crea_num_compte ();

            $solde1=readline("Veuillez saisir le solde du compte courant : ");
            $compte["solde_CC"]=$solde1; 

            $dec1=readline("Découvert autorisé (o/n) ");
            $compte["decouvert_CC"]=$dec1; 
    
            }
            elseif ($choix_compte==2) {
            $choix_compte="LA";
            $compte["type2_compte_client"] = $choix_compte;
            $compte["num_compte_LA"]=crea_num_compte ();

            $solde2=readline("Veuillez saisir le solde du Livret A : ");
            $compte["solde_LA"]=$solde2; 

            }
            elseif ($choix_compte==3) {
            $choix_compte="PEL";
            $compte["type3_compte_client"] = $choix_compte;
            $compte["num_compte_PEL"]=crea_num_compte ();

            $solde3=readline("Veuillez saisir le solde du Plan Epargne Logement : ");
            $compte["solde_PEL"]=$solde3; 

            }
            if (readline("Voulez-vous saisir un nouveau type de compte ? (O)ui\(N)on : ") == "n") {
            break;
            }

            }
            
        
            $comptes[]=$compte;
            //var_dump ($compte);
            echo ("\n");
            var_dump ($comptes);
            echo ("\n");
            $nom_client=$compte["nom_client"];
            echo ("Le compte de $nom_client a bien été créé");
            echo ("\n");
            echo ("Compte n° $num_compte");
            echo ("\n");
            
        } 
    




    if ($ch === "4") {
        $cherche_num_compte=readline("Veuillez choisir le numéro de compte à rechercher : ");
        foreach ($comptes as $i => $compte) {
           
            if ($cherche_num_compte==$comptes[$i]["num_compte"]) {
                var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;
                echo ("Numéro client : " .$compte["num_client"]);
                echo PHP_EOL;
                echo ("Numéro de compte : " .$compte["num_compte"]);
                echo PHP_EOL;
                echo ("Code agence : " .$compte["code_agence"]);
                echo PHP_EOL;
            }
        }
        

    }




    if ($ch === "5") {
        echo PHP_EOL;
        echo "-------------------------------    SOUS MENU ONGLET 5   ------------------------------------------------\n";
        echo ("1 - Recherche de client par son nom".PHP_EOL);
        echo ("2 - Recherche de client par son numéro de compte".PHP_EOL);
        echo ("3 - Recherche de client par son identifiant".PHP_EOL);
        echo ("\n");
   
    $ch = readline("Faites votre choix : ");

    if ($ch === "1") {

        $cherche_nom_client=readline("Veuillez choisir le nom du client à rechercher : ");
    
        foreach ($clients as $i => $client) {
           
            if ($cherche_nom_client==$clients[$i]["nom_client"]) {
                var_dump ($client);
                echo PHP_EOL;
                echo ("Numéro Client : " .$client["num_client"]);
                echo PHP_EOL;
                echo ("Nom : " .$client["nom_client"]);
                echo PHP_EOL;
                echo ("Prénom : " .$client["prenom"]);
                echo PHP_EOL;
                echo ("Date de naissance : " .$client["naissance"]);
                echo PHP_EOL;
                echo ("Mail : " .$client["email"]);
                echo PHP_EOL;
            }
        }
    }

    if ($ch === "2") {

        $cherche_num_compte=readline("Veuillez choisir le numéro de compte du client à rechercher : ");
    
        foreach ($comptes as $i => $compte) {
           
            if ($cherche_num_compte==$comptes[$i]["num_compte"]) {
                //var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;
                echo ("Numéro client : " .$compte["num_client"]);
                echo PHP_EOL;
                echo ("Numéro de compte : " .$compte["num_compte"]);
                echo PHP_EOL;
                echo ("Code agence : " .$compte["code_agence"]);
                echo PHP_EOL;
            }
        }

        
    }

    if ($ch === "3") {

        $cherche_num_client=readline("Veuillez choisir le numéro du client à rechercher : ");
    
        foreach ($clients as $i => $client) {
           
            if ($cherche_num_client==$clients[$i]["num_client"]) {
                var_dump ($client);
                echo PHP_EOL;
                echo ("Numéro Client : " .$client["num_client"]);
                echo PHP_EOL;
                echo ("Nom : " .$client["nom_client"]);
                echo PHP_EOL;
                echo ("Prénom : " .$client["prenom"]);
                echo PHP_EOL;
                echo ("Date de naissance : " .$client["naissance"]);
                echo PHP_EOL;
                echo ("Mail : " .$client["email"]);
                echo PHP_EOL;
            }
        }
    }

    }



    if ($ch === "6") {
        $cherche_num_client=readline("Pour afficher la liste des comptes d'un client, veuillez saisir son identifiant : ");
        foreach ($comptes as $i => $compte) {
            if ($cherche_num_client==$comptes[$i]["num_client"]) {
                var_dump ($compte);
                echo PHP_EOL;
                echo ("Client : " .$compte["nom_client"]);
                echo PHP_EOL;

                
                echo PHP_EOL;

                if (isset($compte["type1_compte_client"])) {
                    echo ("Compte 1 : ");
                    echo ($compte["type1_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_CC"]);
                    echo (" euros");
                    
                }
                else{
                    
                }

                echo PHP_EOL;

                if (isset($compte["type2_compte_client"])) {
                    echo ("Compte 2 : ");
                    echo ($compte["type2_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_LA"]);
                    echo (" euros");
                   
                }
                else{
                    
                }

                echo PHP_EOL;

                if (isset($compte["type3_compte_client"])) {
                    echo ("Compte 3 : ");
                    echo ($compte["type3_compte_client"]);
                    echo PHP_EOL;
                    echo ("Solde : ");
                    echo ($compte["solde_PEL"]);
                    echo (" euros");
                    
                }
                else{
                    
                }

                

                echo PHP_EOL;

                
            }
        }
    }

    


    if ($ch=="7") {
        
        echo PHP_EOL;

        

        $cherche_num_client=readline("Veuillez choisir le numéro du client à rechercher : ");

        foreach ($clients as $client_encours) {
           
            if ($cherche_num_client==$client_encours["num_client"]) {
                
                $client=$client_encours;
                //print_r ($client);
                
            }
        }

        foreach ($comptes as $compte_encours) {

            if ($cherche_num_client==$compte_encours["num_client"]);
            
            $compte=$compte_encours;
            //print_r ($compte);
        }

        
        echo PHP_EOL;

        
        $devise=" euros";
        
        $tmp1=[];
        $tmp2=[];
        $tmp3=[];
        foreach ($compte as $key => $value) { 
            
        }
        
        if (isset($compte["type1_compte_client"])) {
            $tmp1["num_compte_CC"]=$compte["num_compte_CC"];
            $tmp1["solde_CC"]=$compte["solde_CC"].$devise;  
            if ($tmp1["solde_CC"]>=0) {
                $tmp1["smiley1"]=":-)";
            }
            else {
                $tmp1["smiley1"]=":-(";
            }
        }
        else{   
        }
        
        if (isset($compte["type2_compte_client"])) {
            $tmp2["num_compte_LA"]=$compte["num_compte_LA"];
            $tmp2["solde_LA"]=$compte["solde_LA"].$devise;
            if ($tmp2["solde_LA"]>=0) {
                $tmp2["smiley2"]=":-)";
            }
            else {
                $tmp2["smiley2"]=":-(";
            }
        }
        else{   
        }
        
        if (isset($compte["type3_compte_client"])) {
            $tmp3["num_compte_PEL"]=$compte["num_compte_PEL"];
            $tmp3["solde_PEL"]=$compte["solde_PEL"].$devise;
            if ($tmp3["solde_PEL"]>=0) {
                $tmp3["smiley3"]=":-)";
            }
            else {
                $tmp3["smiley3"]=":-(";
            }
        }
        else{   
        }
        
        $car1="Numéro client : ";
        $car2="Nom : ";
        $car3="Prénom : ";
        $car4="Date de naissance : ";
        $car5="Mail : ";
        foreach ($client as $key => $value) { 
            
        }
        $client["num_client"]=$car1.$client["num_client"];
        $client["nom_client"]=$car2.$client["nom_client"];
        $client["prenom"]=$car3.$client["prenom"];
        $client["naissance"]=$car4.$client["naissance"];
        $client["email"]=$car5.$client["email"];
        
        
        
        
        $tmp1=implode("                                   ",$tmp1);
        $tmp2=implode("                                   ",$tmp2);
        $tmp3=implode("                                   ",$tmp3);
        
        
        $tmp=implode("\n",$client);
        
        
        
        echo PHP_EOL;
        $titre="                                            Fiche client";
        $ligne="-----------------------------------------------------------------------------------------------";
        $titre2="Liste de compte";
        $titre3="Numéro de compte                              Solde";
        
        
        
        $tmp=$titre."\n"."\n".$tmp."\n"."\n".$ligne."\n".$titre2."\n".$ligne."\n".$titre3."\n".$ligne."\n".$tmp1."\n".$tmp2."\n".$tmp3;
        
        
        file_put_contents('impression.txt', $tmp);
        echo ("Les infos client sont imprimés dans le fichier impression.txt (même dossier)");
        echo PHP_EOL;
        echo PHP_EOL;




    }

    
    if ($ch=="8") {
        echo ("Au revoir");
        exit;
    }




}


?>